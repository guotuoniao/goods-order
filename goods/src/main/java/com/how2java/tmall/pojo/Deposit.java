package com.how2java.tmall.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "deposit")
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer"})
public class Deposit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "deposit_id")
    public int id;

    @Column(name = "money")
    public float money;

    @Column(name = "user_id")
    public float userId;


    @CreatedDate
    @Column(name = "create_time")
    public Date createTime;
}
