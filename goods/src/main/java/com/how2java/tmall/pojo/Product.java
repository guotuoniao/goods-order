package com.how2java.tmall.pojo;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "goods")
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer"})
@Document(indexName = "tmall_springboot", type = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "goods_id")
    int id;

    @Column(name = "goods_name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "goods_type_id")
    private Category category;

    //如果既没有指明 关联到哪个Column,又没有明确要用@Transient忽略，那么就会自动关联到表对应的同名字段

    private String subTitle;

    @Column(name = "goods_evaluate")
    private float originalPrice;

    @Column(name = "goods_now_price")
    private float promotePrice;

    @Column(name = "goods_click")
    private Integer goodsClick;

    @Transient
    private int cilck;

    private int stock;

    //上架时间,自动填充
    @TableField(fill = FieldFill.INSERT)
    @Column(name = "goods_start")
    private Date createDate;

    //下架时间默认也自动改填充,createDate+3天
    @TableField(fill = FieldFill.INSERT)
    @Column(name = "goods_end")
    private Date endDate;

    //拍品位置
    @Column(name = "goods_pos")
    private String goodsPos;

    private Integer deleted;

    //将不需要序列化的属性前添加关键字transient
    @Transient
    private ProductImage firstProductImage;
    @Transient
    private List<ProductImage> productSingleImages;
    @Transient
    private List<ProductImage> productDetailImages;
    @Transient
    private int reviewCount;
    @Transient
    private int saleCount;
    @Transient
    private List<HistoryEntity> historyEntityList;
}
