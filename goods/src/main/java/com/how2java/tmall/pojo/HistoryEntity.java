package com.how2java.tmall.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.apache.ibatis.annotations.Insert;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

/**
 * @author csd
 * @email csd@gmail.com
 * @date 2020-05-10 00:51:53
 */
@Data
@Entity
@Table(name = "goods_history")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer"})
public class HistoryEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "goods_history_id")
    private Integer goodsHisId;
    /**
     *
     */
    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "goods_price")
    private float goodsPrice;
    /**
     */
    @Column(name = "goods_id")
    private Integer goodsId;
    /**
     *创建时间按添加时自动填充
     */
    @CreatedDate
    @LastModifiedDate
    @Column(name = "create_time")
    private Date createTime;


}
