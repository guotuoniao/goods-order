package com.how2java.tmall.service;

import com.how2java.tmall.dao.HistoryDao;
import com.how2java.tmall.pojo.HistoryEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author csd
 * @email csd@gmail.com
 * @date 2020-05-10 00:51:53
 */
@Service
public class HistoryService {
    @Autowired
    HistoryDao historyDao;

    //todo 插入出价记录
    //todo 查找出价记录
    public List<HistoryEntity> findGoodsHistoryByGoodsId(Integer goodsId) {

        List<HistoryEntity> byGoodsId = historyDao.findByGoodsId(goodsId);
        //todo 暂时不赋值直接返回
       /* for (HistoryEntity h : byGoodsId) {
            //、包装类
            //在springBoot 2.0 中，
            // findById(id) 的返回值是Optional,Optional 是一个包装类，是对user 进行包装，在返回User的时候需要使用get() 方法。
//如果想无论如何都有一个返回，那么就用findOne,否则使用getOne。 只是在2.0版本中没有findOne(id) 但是有 findById(id) 只是要稍微处理一下，需要用get()方法。
            User one = userDAO.getOne(h.getUserId());
            //遍历list 查used_id 为username赋值
            if(one==null){
                h.setUsername("匿名");
            }else {
                h.setUsername(one.getName());
            }
        }*/
        return byGoodsId;
    }

    public void add(HistoryEntity historyEntity) {
        historyDao.save(historyEntity);
    }
}

