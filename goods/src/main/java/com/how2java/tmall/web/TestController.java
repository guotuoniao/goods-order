package com.how2java.tmall.web;

import com.how2java.tmall.feign.OrderFeignService;
import com.how2java.tmall.pojo.User;
import com.how2java.tmall.service.UserService;
import com.how2java.tmall.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class TestController {
    @GetMapping(value = "/testC")
    public String editCategory() {
        return "test";
    }

    @GetMapping(value = "/index")
    public String indexPage() {
        return "admin/index";
    }

    @Autowired
    OrderFeignService orderFeignService;

    @GetMapping(value = "/feigne")
    @ResponseBody
    public String testF() {
        String str = orderFeignService.orderController();
        System.out.println("=====>>" + str);
        return str;
    }

    @GetMapping(value = "/user")
    public String testUser() {
        return "/user/detail";
    }

    @GetMapping(value = "/userdetail")
    @ResponseBody
    public User userDetail() {
        User user = new User();
        user.setId(10);
        user.setName("zs");
        return user;
    }

}
