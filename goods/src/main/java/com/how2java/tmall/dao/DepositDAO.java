package com.how2java.tmall.dao;

import com.how2java.tmall.pojo.Deposit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepositDAO extends JpaRepository<Deposit, Integer> {

}
