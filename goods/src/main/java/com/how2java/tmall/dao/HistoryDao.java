package com.how2java.tmall.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.how2java.tmall.pojo.HistoryEntity;
import com.how2java.tmall.pojo.Order;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * 
 * 
 * @author csd
 * @email csd@gmail.com
 * @date 2020-05-10 00:51:53
 */
@Mapper
public interface HistoryDao extends JpaRepository<HistoryEntity, Integer> {

    List<HistoryEntity> findByGoodsId(Integer goodsId);
}
