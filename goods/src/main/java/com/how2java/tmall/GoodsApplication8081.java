package com.how2java.tmall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.how2java.tmall.feign")
@SpringBootApplication
@EnableJpaAuditing
public class GoodsApplication8081 {
    public static void main(String[] args) {
        SpringApplication.run(GoodsApplication8081.class, args);
    }
}
