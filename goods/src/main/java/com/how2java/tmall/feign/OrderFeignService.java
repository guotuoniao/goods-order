package com.how2java.tmall.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("order-service")//写需要被调用的服务名
public interface OrderFeignService {
    @GetMapping("/order")
    public String orderController();
}
