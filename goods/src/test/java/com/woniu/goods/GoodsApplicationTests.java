package com.woniu.goods;

import com.how2java.tmall.GoodsApplication8081;
import com.how2java.tmall.dao.DepositDAO;
import com.how2java.tmall.dao.HistoryDao;
import com.how2java.tmall.pojo.Deposit;
import com.how2java.tmall.pojo.HistoryEntity;
import com.how2java.tmall.util.RedisUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;


@SpringBootTest(classes = GoodsApplication8081.class)
class GoodsApplicationTests {
   /* @Autowired
    OrderItemDAO orderItemDAO;

    @Test
    void contextLoads() {
        User user = new User();
        user.setId(3);
        List<OrderItem> items = orderItemDAO.findByUserAndOrderIsNull(user);
        for (OrderItem i : items) {
            System.out.println(i);
        }
    }*/

    @Autowired
    StringRedisTemplate stringRedisTemplate;
    @Autowired
    RedisUtil redisUtil;

    @Test
    void redisTest() {
        stringRedisTemplate.opsForValue().set("2", "2020年5月9日23:35:05");
//        for (int i = 0; i < 10; i++) {
//            long pid = redisUtil.incr("pid", 1);
//            System.out.println(pid);
//        }
    }

    @Autowired
    HistoryDao historyDao;

    @Test
    void testDao() {
       /* List<HistoryEntity> all = historyDao.findAll();
        System.out.println(all);*/
        HistoryEntity history = new HistoryEntity();
        history.setGoodsHisId(1);
        history.setGoodsId(1);
        history.setUserId(87);
        history.setGoodsPrice(7.199f);
        historyDao.save(history);
    }
    @Autowired
    DepositDAO depositDAO;
    @Test
    void testdeposDao() {
//        List<Deposit> all = depositDAO.findAll();
//        System.out.println(all);
        Deposit deposit = new Deposit();
        deposit.setMoney(12.3f);
        depositDAO.save(deposit);
    }
}
