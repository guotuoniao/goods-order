package com.woniu.dao;

import com.woniu.entity.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author csd
 * @email csd@gmail.com
 * @date 2020-05-08 11:28:27
 */
@Mapper
public interface UserDao extends BaseMapper<UserEntity> {
	
}
