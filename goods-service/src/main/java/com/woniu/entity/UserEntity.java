package com.woniu.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author csd
 * @email csd@gmail.com
 * @date 2020-05-08 11:28:27
 */
@Data
@TableName("user")
public class UserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer userId;
	/**
	 * 
	 */
	private String username;
	/**
	 * 
	 */
	private String password;
	/**
	 * 
	 */
	private String loginname;
	/**
	 * 
	 */
	private String nickName;
	/**
	 * 
	 */
	private String phone;
	/**
	 * 
	 */
	private String salt;
	/**
	 * 
	 */
	private Date created;
	/**
	 * 
	 */
	private Integer integral;

}
