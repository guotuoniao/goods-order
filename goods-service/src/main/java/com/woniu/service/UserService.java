package com.woniu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.woniuxy.common.utils.PageUtils;
import com.woniu.entity.UserEntity;

import java.util.Map;

/**
 * 
 *
 * @author csd
 * @email csd@gmail.com
 * @date 2020-05-08 11:28:27
 */
public interface UserService extends IService<UserEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

