package com.woniu.orderservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrderServiceApplication9000 {

    public static void main(String[] args) {
        SpringApplication.run(OrderServiceApplication9000.class, args);
    }

}
