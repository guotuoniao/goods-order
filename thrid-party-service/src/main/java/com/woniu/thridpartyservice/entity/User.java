package com.woniu.thridpartyservice.entity;

import lombok.Data;

@Data
public class User {

    private Integer userId;

    private String password;
    private String name;
    private String salt;
    //头像
    private String avatar;

    private Integer openId;

}
