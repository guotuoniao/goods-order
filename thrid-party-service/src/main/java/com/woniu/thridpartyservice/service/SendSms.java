package com.woniu.thridpartyservice.service;

import org.springframework.stereotype.Service;

import java.util.Map;

public interface SendSms {
    public boolean send(String phoneNum, String templateCode, Map<String, Object> code);
}
