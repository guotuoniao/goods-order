package com.woniu.thridpartyservice.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.thridpartyservice.entity.User;

public interface UserMapper extends BaseMapper<User> {

}
