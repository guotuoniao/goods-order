package com.woniu.thridpartyservice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.woniu.thridpartyservice.dao")
@SpringBootApplication
public class ThridPartyServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThridPartyServiceApplication.class, args);
    }

}
