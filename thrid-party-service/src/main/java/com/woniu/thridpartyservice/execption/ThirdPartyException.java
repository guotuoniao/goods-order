package com.woniu.thridpartyservice.execption;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

@Data
@AllArgsConstructor
@NoArgsConstructor
/**
 * 异常类应该放在common中//TODO
 */
public class ThirdPartyException extends Throwable {
    private Integer code;//状态码
    private String msg;//异常信息
}
