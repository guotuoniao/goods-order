package com.woniu.thridpartyservice.controller;

import com.woniu.thridpartyservice.utils.JwtUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@CrossOrigin
@RestController
public class TestController {
    @GetMapping("/test")
    public String test(HttpServletRequest request) {
        System.out.println("request为" + request);
        String jwtToken = JwtUtils.getMemberIdByJwtToken(request);
        System.out.println("jwtToken======>" + jwtToken);
        return jwtToken;
    }

}
