package com.woniu.thridpartyservice;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.woniu.thridpartyservice.dao.UserMapper;
import com.woniu.thridpartyservice.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;

import javax.print.DocFlavor;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@SpringBootTest
class ThridPartyServiceApplicationTests {

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Test
    void testSms() {
        DefaultProfile profile =
                DefaultProfile.getProfile("cn-hangzhou",
                        "LTAI4FrQfkuosM1eACdSqPYh",
                        "twYt7wXvgZdU41qXLDnK45qBdfp8kD");
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");//不动
        request.setVersion("2017-05-25");//不动
        request.setAction("SendSms");

        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", "18428377301");
        request.putQueryParameter("TemplateCode", "SMS_189521270");
        //短信签名名称。请在控制台签名管理页面签名名称一列查看。说明 必须是已添加、并通过审核的短信签名。
        request.putQueryParameter("SignName", "蜗牛拍卖中心");
//        request.putQueryParameter("Action", "发送短信消息");
        HashMap<String, Object> map = new HashMap<>();

        map.put("code", 12313);
        request.putQueryParameter("TemplateParam", JSONObject.toJSONString(map));
        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
        } catch (ClientException e) {
            e.printStackTrace();
        }
    }

    @Test
    void test() {
        System.out.println("?");
    }

    @Test
    void testRedis() {
 /*       String code = stringRedisTemplate.opsForValue().get("code");
        if (code!=null) {
            System.out.println(code);
        }*/

        String code = UUID.randomUUID().toString().substring(0, 4);
        System.out.println(code);
        stringRedisTemplate.opsForValue().set("phone", code, 120, TimeUnit.SECONDS);
        System.out.println("存储结束");
    }
    @Test
    void redisTest() {
        stringRedisTemplate.opsForValue().set("1","1");
    }
}
